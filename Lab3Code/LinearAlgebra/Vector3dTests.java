// Oleksandr Sologub, 2034313
package LinearAlgebra;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;
public class Vector3dTests {
    Vector3d vector = new Vector3d(1, 2, 3);
    Vector3d eVector = new Vector3d(2, 3, 4);

    @Test
    public void testGetX() {
        double ex = vector.getX();
        assertEquals(1, ex);
    }
    @Test
    public void testGetY() {
        double ey = vector.getY();
        assertEquals(2, ey);
    }
    @Test
    public void testGetZ() {
        double ez = vector.getZ();
        assertEquals(3, ez);
    }
    @Test
    public void testMagn() {
        double magn = vector.magnitude();
        assertEquals(Math.sqrt(14), magn);
    }
    @Test
    public void testDot() {
        double eDot = vector.dotProduct(eVector);
        assertEquals(20, eDot);
    }
    @Test
    public void testAdd() {
        Vector3d result = vector.add(eVector);
        double vectx=result.getX();
        double vecty=result.getY();
        double vectz=result.getZ();
        assertEquals(3, vectx);
        assertEquals(5, vecty);
        assertEquals(7, vectz);
    }
}