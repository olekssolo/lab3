// Oleksandr Sologub, 2034313
package LinearAlgebra;
import java.lang.Math;
public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }
    public double getX() {
        return this.x;
    }
    public double getY() {
        return this.y;
    }
    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        double magn = Math.sqrt((x*x)+(y*y)+(z*z));
        return magn;
    }
    public double dotProduct(Vector3d vect) {
        double dot = (x*vect.getX())+(y*vect.getY())+(z*vect.getZ());
        return dot;
    }
    public Vector3d add(Vector3d vect) {
        Vector3d newVect = new Vector3d(this.x+vect.getX(), this.y+vect.getY(), this.z+vect.getZ());
        return newVect;
    }
}

